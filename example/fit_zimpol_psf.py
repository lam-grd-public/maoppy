#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 11:06:34 2019

Fit a VLT/SPHERE/Zimpol PSF with the Psfao model.
You need a Zimpol PSF in FITS format beforehand.

@author: rfetick
"""

from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np

from maoppy.utils import imcenter, circavgplt
from maoppy.psfmodel import Psfao
from maoppy.psffit import psffit
from maoppy.instrument import zimpol

# %% PARAMETERS TO MODIFY
psf_path = "path/to/your/zimpol_psf.fits" # set the path to your PSF *.fits file
filt = zimpol.filters["N_R"]  # ZIMPOL filter
npix = 512 # size of PSF for fitting [pixels]. The larger the better

r0 = 0.18 # Fried parameter [m]
moff_C = 1e-2 # PSD constant [rad²m²]
moff_A = 2. # PSD Moffat variance [rad²]
moff_alpha = 5e-2 # PSD Moffat alpha_x [1/m]
moff_ratio = 1.0 # PSD Moffat ellipticity
moff_theta = 0. # PSF major axis rotation [rad]
moff_beta = 1.6 # PSD Moffat beta power law

# %% READ PSF
fitsPSF = fits.open(psf_path)
hdr = fitsPSF[0].header
psf = fitsPSF[0].data

psf = imcenter(psf, (npix,npix), maxi=True) * zimpol.gain

# %% FIT PSFAO model
wvl = filt[0]
samp = zimpol.samp(wvl)

x0 = [r0,moff_C,moff_A,moff_alpha,moff_ratio,moff_theta,moff_beta]
fixed = [False,False,False,False,True,True,False]

weights = np.ones_like(psf)

print("PSFAO fitting (please wait)")
model = Psfao((npix,npix), system=zimpol, samp=samp)
out = psffit(psf, model, x0, weights=weights, fixed=fixed)

out.x[5] = out.x[5]%np.pi # make angle between 0 and PI

xout = out.x
amp = out.flux_bck[0]
bck = out.flux_bck[1]
DX = out.dxdy[0]
DY = out.dxdy[1]

fitao = amp * out.psf + bck

# %% DISPLAY RESULTS
label = ["r0","C","A","alpha","ellip","theta","beta"]
unit = ["m","rad²m²","rad²","1/m","-","rad","-"]

fmt = "%12s%12s%12.3f%12.3f"
print("-"*48)
print(("%12s"*4)%("Parameter","Unit","Input","Output"))
print("-"*48)
for i in range(len(label)):
    print(fmt%(label[i],unit[i],x0[i],out.x[i]))
print("-"*48)
print(fmt%("dx","pix",0,DX))
print(fmt%("dy","pix",0,DY))
print("%12s%12s%12u%12u"%("flux","photon",0,amp))
print(fmt%("background","photon",0,bck))
print("-"*48)

#%% PLOT RESULTS!

vmin,vmax = np.arcsinh([psf.min(),psf.max()])

plt.figure(1)
plt.clf()

plt.subplot(131)
plt.imshow(np.arcsinh(psf),vmin=vmin,vmax=vmax)
plt.title("Zimpol")

plt.subplot(132)
plt.imshow(np.arcsinh(fitao),vmin=vmin,vmax=vmax)
plt.title("PSFAO")

plt.subplot(133)
plt.imshow(np.arcsinh(psf-fitao),vmin=vmin,vmax=vmax)
plt.title("Residual")



plt.figure(2)
plt.clf()

x,y = circavgplt(psf)
plt.semilogy(x, y, label="Zimpol")

x,y = circavgplt(fitao)
plt.semilogy(x, y, label="PSFAO")

plt.legend()
plt.grid()
plt.xlabel("Radius [pix]")
plt.ylabel("PSF [photon]")
plt.xlim(-npix//2,npix//2)