# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 17:16:53 2021

Run profiler on this script to find computation bottleneck

Results:
    31 Aug 2021 - precompute _otfDiffraction
        time drops from 160 to 122 ms/iter (100% -> 76%)
    09 Sep 2021 - remove some fftshift, use scipy.fft
        time drops from 110 to 97 ms/iter (100% -> 88%)
    17 Nov 2021 - precompute fx2, fy2 and fxy for moffat
        time drops from 95 to 83 ms/iter (100% -> 87%)

@author: rfetick
"""

from time import time
from maoppy.psfmodel import Psfao
from maoppy.instrument import muse_nfm

npix = 128 # pixel size of PSF
wvl = 600*1e-9 # wavelength [m]
niter = 50 # number of PSF to generate

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((npix,npix), system=muse_nfm, samp=samp)

#%% Choose parameters and compute PSF
r0 = 0.15 # Fried parameter [m]
bck = 1e-5 # Phase PSD background [rad² m²]
amp = 5.0 # Phase PSD Moffat amplitude [rad²]
alpha = 0.1 # Phase PSD Moffat alpha [1/m]
ratio = 1.2
theta = 3.14/4
beta = 1.6 # Phase PSD Moffat beta power law

param = [r0,bck,amp,alpha,ratio,theta,beta]

t0 = time()
for i in range(niter):
    psf = Pmodel(param, dx=0, dy=0)
t1 = time()
print("Elapsed: %u ms/iter"%round((t1-t0)*1e3/niter))