#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 18:45:28 2020

-------------------
SHOW FITTING POSSIBILITY WITH PSFAO and the MAOPPY LIBRARY
-------------------

Generate an observation of a star with MUSE-NFM, at a given wavelength
Fit the simulated image with the Psfao model
Plot results and display fitting accuracy

@author: rfetick
"""

import matplotlib.pyplot as plt
import numpy as np

from maoppy.psfmodel import Psfao
from maoppy.psffit import psffit
from maoppy.instrument import muse_nfm

npix = 128 # pixel size of PSF
wvl = 800*1e-9 # wavelength [m]

flux = 1e4
ron = muse_nfm.ron
bckgd = 1.0

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((npix,npix), system=muse_nfm, samp=samp)

#%% Generate a MUSE-NFM PSF
r0 = 0.12 # Fried parameter [m]
b = 1e-2 # Phase PSD background [rad² m²]
amp = 3.0 # Phase PSD Moffat amplitude [rad²]
alpha = 0.1 # Phase PSD Moffat alpha [1/m]
ratio = 1.2 # major/minor axis
theta = np.pi/3 # angle [rad]
beta = 1.6 # Phase PSD Moffat beta power law

param = [r0,b,amp,alpha,ratio,theta,beta]

dxtrue = 0
dytrue = 0

psf = Pmodel(param,dx=dxtrue,dy=dytrue)

noise = ron*np.random.randn(npix,npix)
image = flux*psf + bckgd + noise

#%% Fit the PSF with Psfao
guess = [0.145,2e-2,1.2,alpha,1,0,1.5]
w = np.ones_like(image)/ron**2.0
fixed = [False,False,False,True,False,False,False]

out = psffit(image, Pmodel, guess, weights=w, fixed=fixed)

flux_fit, bck_fit = out.flux_bck
fitao = flux_fit*out.psf + bck_fit

#%% Plots
print(42*'-')
print("%9s %10s %10s %10s"%('','TRUE','ESTIM','STD'))
print(42*'-')
for i in range(len(param)):
    if not fixed[i]:
        print("%9s %10.3f %10.3f %10.3f"%(Pmodel.param_name[i],param[i],out.x[i],out.x_std[i]))
# print("%9s %10.2f %10.2f %10.2f"%('r0 [cm]',r0*100,out.x[0]*100,out.x_std[0]*100))
# print("%9s %10.2f %10.2f %10.2f"%('A [rad²]',amp,out.x[2],out.x_std[2]))
print(' -'*21)
print("%9s %10.3g %10.3g"%('Flux [e-]',flux,flux_fit))
print("%9s %10.3g %10.3g"%('Bck [e-]',bckgd,bck_fit))
print("%9s %10.2f %10.2f %10.2f"%('dx [pix]',dxtrue,out.dxdy[0],out.dxdy_std[0]))
print("%9s %10.2f %10.2f %10.2f"%('dy [pix]',dytrue,out.dxdy[1],out.dxdy_std[1]))
print(42*'-')

vmin,vmax = np.arcsinh([image.min(),image.max()])

plt.figure(1)
plt.clf()

plt.subplot(131)
plt.imshow(np.arcsinh(image),vmin=vmin,vmax=vmax)
plt.axis('off')
plt.title('MUSE-NFM simu')

plt.subplot(132)
plt.imshow(np.arcsinh(fitao),vmin=vmin,vmax=vmax)
plt.axis('off')
plt.title('Psfao fit')

plt.subplot(133)
plt.imshow(np.arcsinh(image-fitao))
plt.axis('off')
plt.title('residuals')

