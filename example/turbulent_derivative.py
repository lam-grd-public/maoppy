# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:56:24 2021

@author: rfetick
"""

import matplotlib.pyplot as plt
import numpy as np
import copy

from maoppy.psfmodel import Turbulent
from maoppy.instrument import muse_nfm

Npix = 128 # pixel size of PSF
wvl = 600*1e-9 # wavelength [m]

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Turbulent((Npix,Npix),system=muse_nfm,samp=samp)

#%% Choose parameters
r0 = 0.15 # Fried parameter [m]
L0 = 20 # external length [m]

param = [r0,L0]

#%% Compute PSD derivative
psd,_,g,_ = Pmodel.psd(param,grad=True)

gnum = 0*g
eps = 1e-5
for i in range(len(param)):
    dp = copy.copy(param)
    dp[i] += eps
    psd2,_ = Pmodel.psd(dp)
    gnum[i,...] = (psd2-psd)/eps

#%% Plot results
names = ['r0','L0']

plt.figure(1)
plt.clf()
for i in range(len(param)):
    plt.subplot(2,len(param),i+1)
    plt.pcolormesh(np.arcsinh(g[i,...]))
    plt.axis('image')
    plt.axis('off')
    plt.title(names[i])
    plt.colorbar(orientation='horizontal')
    
    plt.subplot(2,len(param),i+1+len(param))
    plt.pcolormesh(np.arcsinh(gnum[i,...]))
    plt.axis('image')
    plt.axis('off')
    #plt.title(names[i])
    plt.colorbar(orientation='horizontal')
    
#%% Compute PSF derivative
psf,g = Pmodel(param,grad=True)

gnum = 0*g
eps = 1e-5
for i in range(len(param)):
    dp = copy.copy(param)
    dp[i] += eps
    psf2 = Pmodel(dp)
    gnum[i,...] = (psf2-psf)/eps

#%% Plot results
names = ['r0','L0']

plt.figure(2)
plt.clf()
for i in range(len(param)):
    plt.subplot(2,len(param),i+1)
    plt.pcolormesh(g[i,...])
    plt.axis('image')
    plt.axis('off')
    plt.title(names[i])
    plt.colorbar(orientation='horizontal')
    
    plt.subplot(2,len(param),i+1+len(param))
    plt.pcolormesh(gnum[i,...])
    plt.axis('image')
    plt.axis('off')
    #plt.title(names[i])
    plt.colorbar(orientation='horizontal')