#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 21:07:53 2020

--------------
Show the possibility of fitting a PSF with Psfao on a small field of view
and to retrieve the PSF shape on an increased field of view.
--------------
This script is a more complex version of 'fit_muse_simulated_psf.py'
The other one should be run first for better understanding.
--------------

Generate an observation of a star with MUSE-NFM, at a given wavelength
Crop the image to simulate a limited field of view
Fit the simulated image with the Psfao model
Plot results and display fitting accuracy
    Full images are shown
    Red rectangles indicate the reduced field of view used for fitting

@author: rfetick
"""

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np

from maoppy.psfmodel import Psfao
from maoppy.psffit import psffit
from maoppy.instrument import muse_nfm

npix = 128 # pixel size of PSF
wvl = 600*1e-9 # wavelength [m]

flux = 1e4
ron = muse_nfm.ron
bckgd = 1.0

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((npix,npix), system=muse_nfm, samp=samp)

#%% Generate a MUSE-NFM PSF
r0 = 0.15 # Fried parameter [m]
b = 1e-2 # Phase PSD background [rad² m²]
amp = 3.0 # Phase PSD Moffat amplitude [rad²]
alpha = 0.1 # Phase PSD Moffat alpha [1/m]
ratio = 1.0 # Phase PSD Moffat ellipticity
theta = 0.0 # Phase PSD Moffat angle
beta = 1.6 # Phase PSD Moffat beta power law

param = [r0,b,amp,alpha,ratio,theta,beta]

psf = Pmodel(param,dx=0,dy=0)

noise = ron*np.random.randn(npix,npix)
image = flux*psf + bckgd + noise

#%% Crop the image
ncrop = 40
im_crop = image[npix//2-ncrop//2:npix//2+ncrop//2,
                npix//2-ncrop//2:npix//2+ncrop//2]

#%% Fit the cropped image with Psfao
guess = [0.145,2e-2,1.2,0.08,ratio,theta,1.5]
w = np.ones_like(im_crop)/ron**2.0
fixed = [False,False,False,False,True,True,False]

out = psffit(im_crop, Pmodel, guess, weights=w, fixed=fixed, npixfit=npix)

flux_fit, bck_fit = out.flux_bck
fitao = flux_fit*out.psf + bck_fit

#%% Plots
print(31*'-')
print("%9s %10s %10s"%('','MUSE-NFM','Psfao'))
print(31*'-')
print("%9s %10.4g %10.4g"%('Flux [e-]',flux,flux_fit))
print("%9s %10.4g %10.4g"%('Bck [e-]',bckgd,bck_fit))
print("%9s %10.2f %10.2f"%('r0 [cm]',r0*100,out.x[0]*100))
print("%9s %10.2f %10.2f"%('A [rad²]',amp,out.x[2]))
print(31*'-')

corner = (npix//2-ncrop//2,npix//2-ncrop//2)
r1 = Rectangle(corner,ncrop,ncrop,linewidth=2,edgecolor='r',facecolor='none')
r2 = Rectangle(corner,ncrop,ncrop,linewidth=2,edgecolor='r',facecolor='none')
r3 = Rectangle(corner,ncrop,ncrop,linewidth=2,edgecolor='r',facecolor='none')

vmin,vmax = np.arcsinh([image.min(),image.max()])

plt.figure(1)
plt.clf()

ax1 = plt.subplot(131)
plt.imshow(np.arcsinh(image),vmin=vmin,vmax=vmax)
plt.axis('off')
plt.title('MUSE-NFM simu')
ax1.add_patch(r1) # actual field of view for fitting

ax2 = plt.subplot(132)
plt.imshow(np.arcsinh(fitao),vmin=vmin,vmax=vmax)
plt.axis('off')
plt.title('Psfao fit')
ax2.add_patch(r2) # actual field of view for fitting

ax3 = plt.subplot(133)
plt.imshow(np.arcsinh(image-fitao))
plt.axis('off')
plt.title('residuals')
ax3.add_patch(r3) # actual field of view for fitting
