# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:20:53 2021

Show the analytical and numerical derivatives for Psfao

@author: rfetick
"""

import matplotlib.pyplot as plt
import numpy as np
import copy

from maoppy.psfmodel import Psfao
from maoppy.instrument import muse_nfm

Npix = 64 # pixel size of PSF
wvl = 650*1e-9 # wavelength [m]

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((Npix,Npix),system=muse_nfm,samp=samp)

#%% Choose parameters
r0 = 0.15 # Fried parameter [m]
bck = 1e-5 # Phase PSD background [rad² m²]
amp = 3.0 # Phase PSD Moffat amplitude [rad²]
alpha = 0.8 # Phase PSD Moffat alpha [1/m]
ratio = 1.2
theta = np.pi/4
beta = 1.6 # Phase PSD Moffat beta power law

param = [r0,bck,amp,alpha,ratio,theta,beta]

#%% Compute PSD derivative
psd,_,g,_ = Pmodel.psd(param,grad=True)

gnum = 0*g
eps = 1e-5
for i in range(len(param)):
    dp = copy.copy(param)
    dp[i] += eps
    psd2,_ = Pmodel.psd(dp)
    gnum[i,...] = (psd2-psd)/eps

#%% Plot results
names = ['r0','bck','amp','alpha','ratio','theta','beta']

def setplt():
    plt.axis('image')
    plt.axis('off')
    plt.colorbar(orientation='horizontal')


plt.figure(1)
plt.clf()
plt.suptitle("PSD derivatives (top:analytical, middle:numerical, bottom:1e4*difference)")

for i in range(len(param)):
    vmax = np.max(np.abs(g[i,...]))
    
    plt.subplot(3,len(param),i+1)
    plt.pcolormesh(g[i,...],vmin=-vmax,vmax=vmax,cmap='coolwarm')
    plt.title(names[i])
    setplt()
    
    plt.subplot(3,len(param),i+1+len(param))
    plt.pcolormesh(gnum[i,...],vmin=-vmax,vmax=vmax,cmap='coolwarm')
    setplt()
    
    plt.subplot(3,len(param),i+1+2*len(param))
    plt.pcolormesh((gnum[i,...]-g[i,...])*1e4,vmin=-vmax,vmax=vmax,cmap='coolwarm')
    setplt()
    
#%% Compute PSF derivative
psf,g = Pmodel(param,grad=True)

gnum = 0*g
eps = 1e-5
for i in range(len(param)):
    dp = copy.copy(param)
    dp[i] += eps
    psf2 = Pmodel(dp)
    gnum[i,...] = (psf2-psf)/eps

#%% Plot results
plt.figure(2)
plt.clf()
plt.suptitle("PSF derivatives (top:analytical, middle:numerical, bottom:1e4*difference)")

for i in range(len(param)):
    vmax = np.max(np.abs(g[i,...]))
    
    plt.subplot(3,len(param),i+1)
    plt.pcolormesh(g[i,...],vmin=-vmax,vmax=vmax,cmap='coolwarm')
    plt.title(names[i])
    setplt()
    
    plt.subplot(3,len(param),i+1+len(param))
    plt.pcolormesh(gnum[i,...],vmin=-vmax,vmax=vmax,cmap='coolwarm')
    setplt()
    
    plt.subplot(3,len(param),i+1+2*len(param))
    plt.pcolormesh((gnum[i,...]-g[i,...])*1e4,vmin=-vmax,vmax=vmax,cmap='coolwarm')
    setplt()