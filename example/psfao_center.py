# -*- coding: utf-8 -*-
"""
Created on Mon Jul 12 17:59:23 2021

Show the center convention for Psfao
It is at size/2 for even sized arrays.

@author: rfetick
"""


from maoppy.instrument import zimpol
from maoppy.psfmodel import Psfao
import numpy as np
import matplotlib.pyplot as plt

#%% PARAMETERS
npix = 10
samp = 2.0 # you can choose oversampled or undersampled.

#%% BUILD PSF (without turbulence)
psfmodel = Psfao((npix,npix), system=zimpol, samp=samp)

param = {"r0": 1e5,
         "bck": 0,
         "amp": 0,
         "alpha": 1,
         "ratio": 1.0,
         "theta": 0,
         "beta": 1.5}

psf = psfmodel(param)

#%% PLOT RESULT
c = np.where(psf==psf.max())
c = (c[0][0],c[1][0])
print("For size (%u,%u), the center is at (%u,%u)"%(npix,npix,c[0],c[1]))

plt.figure(1)
plt.clf()
plt.imshow(psf)
