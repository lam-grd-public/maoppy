# -*- coding: utf-8 -*-
"""
Created on Fri Jul  9 10:16:23 2021

Create a wrapper around Psfao for GalPak software

@author: rfetick
"""

from maoppy.utils import RAD2ARCSEC
from maoppy.psfmodel import Psfao


class PointSpreadFunction:
    """Dummy class. Replace it with the GalPak implementation"""
    pass


class PsfaoGalpak(Psfao, PointSpreadFunction):
    """
    Implementation of Psfao for GalPak software.
    """
    
    def __init__(self, param, wvl_um, system=None, Lext=10., fixed_k=None):
        """
        Constructor.
        
        Parameters
        ----------
        param : list, tuple, np.array (of 7 elements)
            Vector of parameters as defined in Psfao.
            [r0,C,A,alpha,ratio,theta,beta].
        wvl_um : float
            The wavlength [um] corresponding to the given parameter.
            A theoretical evolution of the parameters is used by default,
            otherwise consider changing the parameters for each wavelength.
        system : maoppy.instrument.Instrument
            The AO system related to the PSF computation.
        """
        npix = (10,10) # will be modified in 'as_image'
        samp = 2.0 # will be modified in 'as_image'
        super().__init__(npix, system=system, Lext=Lext, samp=samp, fixed_k = fixed_k)
        self.param = param # [r0,C,A,alpha,ratio,theta,beta]
        self.wvl_ref_um = wvl_um
        
    
    def as_image(self, for_cube):
        """
        Return PSFAO as a 2D image shaped [for_cube].

        for_cube: HyperspectralCube
            Has additional properties computed and attributed by GalPaK :
                - xy_step (in ")
                - z_step (in µm) -> not used here...
                - z_central (in µm)

        :rtype: ndarray
        """
        self.npix = for_cube.shape[1:]
        self.system._resolution_rad = for_cube.xy_step / RAD2ARCSEC
        wvl_um = for_cube.z_central
        self.samp = self.system.samp(wvl_um*1e-6)
        ww = wvl_um/self.wvl_ref_um # wavelength ratio
        r0,C,A,alpha,ratio,theta,beta = self.param
        p = [r0*ww**(6./5.),C*ww**(-2),A*ww**(-2),alpha,ratio,theta,beta]
        dx = -(1-self.npix[0]%2)/2 # between 4 pixels if even nb of pixels
        dy = -(1-self.npix[1]%2)/2 # between 4 pixels if even nb of pixels
        return self.__call__(p, dx=dx, dy=dy)


###################################################
################### TEST SCRIPT ###################
###################################################

if __name__ == "__main__":
    
    ### PSF PARAMETERS
    r0 = 0.10 # Fried parameter [m]
    C = 1e-2
    A = 0.8 # AO variance [rad²]
    alpha = 1e-2
    ratio = 1.0
    theta = 0.0
    beta = 1.5
    p0 = [r0,C,A,alpha,ratio,theta,beta]
    wvl_um = 0.55 # wavelength at which are given the parameters [um]
    
    ### IMPORT INSTRUMENT
    from maoppy.instrument import muse_wfm
    
    ### CREATE DUMMY CUBE
    class MuseCube:
        def __init__(self, shape, xy_step, z_central):
            self.shape = shape
            self.xy_step = xy_step
            self.z_central = z_central
    
    nwvl = 3 # number of wavelengths in the sub-cube (whatever, not used here)
    nx = 15 # number of pixels on X
    ny = 15 # number of pixels on Y
    xy_step = muse_wfm.resolution_mas*1e-3 # comply with Muse resolution for this dummy cube
    z_central = 0.8 # [um]
    for_cube = MuseCube((nwvl,nx,ny), xy_step, z_central)
    
    ### CREATE PSF MODEL and GET 2D PSF
    pg = PsfaoGalpak(p0, wvl_um, system=muse_wfm)
    psf = pg.as_image(for_cube)
    
    print("PSF energy = %.4f"%psf.sum()) # E=1 up to intfy, so E<1 on the FoV
    
    ### PLOT PSF
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    plt.figure(1)
    plt.clf()
    plt.imshow(psf, norm=LogNorm())
    plt.colorbar()
    plt.title("PSF at %u nm"%(z_central*1e3))
    plt.xlabel("X [pix]")
    plt.ylabel("Y [pix]")
    plt.show()
    
    ### PLOT PSD
    # from maoppy.utils import circavg
    # psd,_ = pg.psd(p0)
    
    # plt.figure(2)
    # plt.clf()
    # plt.loglog(circavg(psd))
    