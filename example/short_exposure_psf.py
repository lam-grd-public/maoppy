"""
Create long and short exposure PSFs.

Warning:
The long exposure PSF is normalized in energy up to infinity.
Short exposure PSFs are normalized in energy in the numerical FoV.
"""

import numpy as np
from maoppy.utils import circavgplt
from maoppy.instrument import zimpol
from maoppy.psfmodel import Psfao
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

#%% PARAMETER

### GENERAL
nx = 512
wvl = zimpol.filters['N_R'][0]
nshort = 10 # number of short exposures to draw

### AO PSD
r0 = 0.15 # Fried parameter [m]
psd_bck = 1e-2 # background in PSD domain [rad²m²]
amp = 1.0 # AO variance [rad²]
alpha = 0.06 # Moffat cutoff [1/m]
ratio = 1.0 # ellipticity ratio
theta = np.pi/4 # rotation angle [rad]
beta = 1.2 # Moffat beta parameter

#%% INIT MODEL
samp = zimpol.samp(wvl)
psfmodel = Psfao((nx,nx), system=zimpol, samp=samp)
param = [r0, psd_bck, amp, alpha, ratio, theta, beta]

long_exposure = psfmodel(param)

short_exposure = np.zeros((nshort,nx,nx))
for i in range(nshort):
    short_exposure[i,...] = psfmodel.short_exposure(param)

#%% PLOT
def setplt(psf):
    xp = (np.arange(nx)-nx/2)*zimpol.resolution_mas
    plt.pcolormesh(xp, xp, psf, norm=LogNorm(vmin=1e-7), cmap='inferno')
    plt.axis('image')
    plt.colorbar()
    plt.xlabel('Position [mas]')


plt.figure(1, figsize=(18,5.5))
plt.clf()

plt.subplot(131)
plt.title('Long exposure')
setplt(long_exposure)

plt.subplot(132)
plt.title('Short exposure #0')
setplt(short_exposure[0,...])

plt.subplot(133)
plt.title('$\\Sigma$ short exposure')
setplt(np.mean(short_exposure, axis=0))


plt.figure(2)
plt.clf()
plt.semilogy(*circavgplt(long_exposure), lw=2, label='Long exp')
plt.semilogy(*circavgplt(np.mean(short_exposure, axis=0)), lw=2, label='Short exp avg')
plt.legend()
plt.grid()
plt.xlabel('Position [pix]')
plt.title('PSF')
