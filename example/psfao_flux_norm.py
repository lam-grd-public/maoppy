# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 10:43:57 2022

Psfao flux can be chosen to be unity on the numerical FoV or up to infinity.
This effect is not important for big FoV (wrt. PSF halo), but significant for small FoV.
For a small FoV:
    - normalizing PSF energy to unity on the FoV leads to shape mismatch
    - normalizing PSF energy up to infinity leads to better shape

@author: rfetick
"""

from maoppy.psfmodel import Psfao
from maoppy.instrument import zimpol
from maoppy.utils import circavgplt
import matplotlib.pyplot as plt

#%% PARAMETERS
npix_small = 180
npix_big = 600
samp = 3

#%% GET PSFs
Psmall = Psfao((npix_small,npix_small), system=zimpol, samp=samp)
Pbig = Psfao((npix_big,npix_big), system=zimpol, samp=samp)

param = {"r0": 0.08,
         "bck": 0,
         "amp": 0.5,
         "alpha": 0.1,
         "ratio": 1.0,
         "theta": 0,
         "beta": 1.5}

psf_small_fovnorm = Psmall(param, fovnorm=True)
psf_small_infnorm = Psmall(param, fovnorm=False)
psf_big_fovnorm = Pbig(param, fovnorm=True)

#%% PLOT
def pltfun(tab):
    x,y = circavgplt(tab)
    nx,ny = tab.shape
    center= len(x)//2
    return x[center-nx//2:center+nx//2], y[center-nx//2:center+nx//2]

plt.figure(1)
plt.clf()
plt.semilogy(*pltfun(psf_small_fovnorm), lw=3, label='small FoV, sum(PSF)=%.2f'%psf_small_fovnorm.sum())
plt.semilogy(*pltfun(psf_small_infnorm), lw=3, label='small FoV, sum(PSF)=%.2f'%psf_small_infnorm.sum())
plt.semilogy(*pltfun(psf_big_fovnorm), lw=2, ls='--', c='k', label='big FoV, sum(PSF)=%.2f'%psf_big_fovnorm.sum())
plt.xlim(-npix_small,npix_small)
plt.grid()
plt.legend()
plt.ylim(1e-6,5e-2)