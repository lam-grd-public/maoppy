#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 15:18:38 2019

Generate a VLT/MUSE-NFM PSF with the Psfao model.

@author: rfetick
"""


import matplotlib.pyplot as plt
import numpy as np

from maoppy.psfmodel import Psfao
from maoppy.instrument import muse_nfm

Npix = 128 # pixel size of PSF
wvl = 600*1e-9 # wavelength [m]

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((Npix,Npix),system=muse_nfm,samp=samp)

#%% Choose parameters and compute PSF
param = {"r0": 0.15,  # Fried parameter [m]
         "bck": 1e-2,  # background [rad² m²]
         "amp": 4.0,  # Moffat amplitude [rad²]
         "alpha": 0.1,  # Moffat alpha [1/m]
         "ratio": 1.2,  # Moffat elongation sqrt(ax/ay)
         "theta": np.pi/4,  # Moffat angle [rad]
         "beta": 1.6}  # Moffat beta power law

psf = Pmodel(param,dx=0,dy=0)
print("Strehl ratio (from OTF): %.1f%%"%(Pmodel.strehlOTF(param)*100))

#%% Plot results
plt.figure(1)
plt.clf()
plt.pcolormesh(np.log(psf))
plt.axis('image')
plt.title("MUSE simulated PSF at wvl=%unm"%(wvl*1e9))