"""
Created on Wed Oct 12 14:57:51 2022

Profile the Psfao function to find computation time bottlenecks

@author: alix.yan, romain.fetick
"""

import cProfile, pstats, io
from maoppy.psfmodel import Psfao
from maoppy.instrument import muse_nfm

npix = 128 # pixel size of PSF
wvl = 600*1e-9 # wavelength [m]
niter = 50 # number of PSF to generate

#%% Initialize PSF model
samp = muse_nfm.samp(wvl) # sampling (2.0 for Shannon-Nyquist)
Pmodel = Psfao((npix,npix), system=muse_nfm, samp=samp)

#%% Choose parameters and compute PSF
r0 = 0.15 # Fried parameter [m]
bck = 1e-5 # Phase PSD background [rad² m²]
amp = 5.0 # Phase PSD Moffat amplitude [rad²]
alpha = 0.1 # Phase PSD Moffat alpha [1/m]
ratio = 1.2
theta = 3.14/4
beta = 1.6 # Phase PSD Moffat beta power law

param = [r0,bck,amp,alpha,ratio,theta,beta]

# The case dx=dy=0 allows to save time by avoiding to compute _otfShift
dx = 0.0
dy = 0.0

pr = cProfile.Profile()
pr.enable()
for i in range(niter):
    psf = Pmodel(param, dx=dx, dy=dy)
pr.disable()

s = io.StringIO()
key = pstats.SortKey.CUMULATIVE # TIME or CUMULATIVE
ps = pstats.Stats(pr, stream=s).strip_dirs().sort_stats(key)
ps.print_stats(15)
print(s.getvalue())
