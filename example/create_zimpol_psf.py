"""
Generate a VLT/ZIMPOL like PSF
"""

import numpy as np
from maoppy.instrument import zimpol, pupil_vlt, petal_mode_vlt
from maoppy.psfmodel import Psfao
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

#%% PARAMETER

### GENERAL
nx = 512
wvl = zimpol.filters['N_R'][0]

### AO PSD
r0 = 0.15 # Fried parameter [m]
psd_bck = 1e-2 # background in PSD domain [rad²m²]
amp = 1.0 # AO variance [rad²]
alpha = 0.06 # Moffat cutoff [1/m]
ratio = 1.0 # ellipticity ratio
theta = np.pi/4 # rotation angle [rad]
beta = 1.2 # Moffat beta parameter

### PUPIL FRAGMENTATION (e.g. LOW WIND EFFECT)
pupil_angle = 0 # pupil spiders angle [rad]
coefs_piston = [2,1,0,1]
coefs_tip = [0,0,0,0]
coefs_tilt = [-1,-1,0,-1]

#%% DEFINE FINE EFFECTS PUPILS

def diffraction_pupil(*args, **kwargs):
    # ignore kwargs
    return pupil_vlt(*args, angle=pupil_angle, oversamp=2)

def fragmented_phase(*args, **kwargs):
    # ignore kwargs
    modes = petal_mode_vlt(*args, angle=pupil_angle, oversamp=2)
    coefs_ptt = np.concatenate((coefs_piston, coefs_tip, coefs_tilt))
    phi = np.zeros(args[0])
    for i in range(12):
        phi += coefs_ptt[i] * modes[i,...]
    return phi

def fragmented_pupil(*args, **kwargs):
    return diffraction_pupil(*args, **kwargs) * np.exp(1j*fragmented_phase(*args, **kwargs))

#%% GET DIFFRACTION PSF (I make sure to remove fragmentation)
samp = zimpol.samp(wvl)
zimpol.pupil = diffraction_pupil
psfmodel = Psfao((nx,nx), system=zimpol, samp=samp)
psf_diffraction = psfmodel.psfDiffraction
psf_diffraction = psf_diffraction/np.sum(psf_diffraction)

#%% CREATE PSF
samp = zimpol.samp(wvl)
zimpol.pupil = fragmented_pupil
psfmodel = Psfao((nx,nx), system=zimpol, samp=samp)
param = [r0, psd_bck, amp, alpha, ratio, theta, beta]
psf = psfmodel(param)
psf = psf/np.sum(psf)
_,var_ao = psfmodel.psd(param)

#%% PRINT
sr = np.max(psf)/np.max(psf_diffraction)
pup = np.abs(fragmented_pupil((nx,nx)))
phi = fragmented_phase((nx,nx))
var_petalling = np.var(phi[pup>0])

print('wavelength     : %4u nm'%(wvl*1e9))
print('WFE petalling  : %4.2f rad²'%var_petalling)
print('WFE turbulence : %4.2f rad²'%var_ao)
print('Strehl ratio   : %4.1f %%'%(sr*100))

#%% PLOT
plt.figure(1, figsize=(15,6))
plt.clf()

plt.subplot(121)
xp = np.linspace(-zimpol.D/2, zimpol.D/2, num=nx)
vmax = np.max(np.abs(phi))
plt.title('Pupil fragmentation [rad]')
plt.pcolormesh(xp, xp, phi, cmap='coolwarm', vmin=-vmax, vmax=vmax)
plt.axis('image')
plt.colorbar()
plt.contour(xp, xp, pup, [0.5])
plt.xlabel('Position [m]')

plt.subplot(122)
xp = (np.arange(nx)-nx/2)*zimpol.resolution_mas
plt.title('PSF')
plt.pcolormesh(xp, xp, psf, norm=LogNorm(vmin=1e-7), cmap='inferno')
plt.axis('image')
plt.colorbar()
plt.xlabel('Position [mas]')
